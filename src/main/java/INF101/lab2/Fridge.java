package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{

    ArrayList<FridgeItem> fridge;
    int maxSize = 20;

    /** 
     * Contains an ArrayList of FridgeItems objects
    */
    public Fridge() {
        fridge = new ArrayList<FridgeItem>();
    }

    @Override
    public int nItemsInFridge() {
        return this.fridge.size();
    }

    @Override
    public int totalSize() {
        return maxSize;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        // for (FridgeItem compareItem : fridge) {
        //     if (compareItem == item) {
        //         return false;
        //     }
        // }
        if (nItemsInFridge()<totalSize()) {
            this.fridge.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (this.fridge.size()==0) {
            throw new NoSuchElementException();
        }
        this.fridge.remove(item);
    }

    @Override
    public void emptyFridge() {
        this.fridge.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredFood = new ArrayList<>();
        for (FridgeItem item : fridge) {
            if (item.hasExpired()) {
                expiredFood.add(item);
            }
        }
        this.fridge.removeAll(expiredFood);
        return expiredFood;
    }
}